/*
  Challenger USB to serial port.

  This is an example on how to flash the onboard ESP8285.
  TinyUSB must be selected for this sketch to work.

  The ESP8285 needs to flashed with the following command:
    esptool.py --port <port> --baud <rate> write_flash -e -fm dout -fs 1MB <filename.bin>
  
*/
#include <Arduino.h>
#ifdef USE_TINYUSB
#include <Adafruit_TinyUSB.h>
#define USE_OVERRIDE_LINE_STATE_CB    1
#endif

// Sets the level of debug info that is printed on the serial output
//   0 = No debug information at all
//   1 = Basic debug information
//   2 = Full debug information
#define DEBUG_LVL     0

#define ESP_PORT      Serial2
#define PC_PORT       Serial
#define DEBUG_PORT    Serial1

#define PACKET_SIZE   64

int led = LED_BUILTIN;           // the PWM pin the LED is attached to
int led_state = 0;
uint32_t baud_rate = 115200;
static int port = -1;
static int words = 0;
uint8_t ch;
char buffer[PACKET_SIZE];
size_t received;

//
// This overrides the stack functionality to capture cdc line state
// changes.
//
#if defined(USE_OVERRIDE_LINE_STATE_CB)
void tud_cdc_line_coding_cb(uint8_t itf, cdc_line_coding_t const* coding) {
  (void) itf;
#if DEBUG_LVL >= 1
  DEBUG_PORT.printf("\n");
#endif    
  if (coding->bit_rate != baud_rate) {

    // flush and change baudrate to ESP8285
    //ESP_PORT.end();
    ESP_PORT.begin(coding->bit_rate);
    baud_rate = coding->bit_rate;
#if DEBUG_LVL >= 1
    DEBUG_PORT.printf("Setting new baudrate to %d ", baud_rate);
#endif    
  }
}

void tud_cdc_line_state_cb(uint8_t itf, bool dtr, bool rts)
{
  (void) itf;  // interface ID, not used

  cdc_line_coding_t coding;
  tud_cdc_get_line_coding(&coding);

  if (rts) {
    digitalWrite(PIN_ESP8285_RST, LOW);
#if DEBUG_LVL >= 1
    DEBUG_PORT.printf("RST=low ");
#endif    
  } else {
    digitalWrite(PIN_ESP8285_RST, HIGH);
#if DEBUG_LVL >= 1
    DEBUG_PORT.printf("RST=high ");
#endif    
  }
    
  if (dtr) {
    digitalWrite(PIN_ESP8285_MODE, LOW);
#if DEBUG_LVL >= 1
    DEBUG_PORT.printf("MODE=low");
#endif    
  } else {
    digitalWrite(PIN_ESP8285_MODE, HIGH);
#if DEBUG_LVL >= 1
    DEBUG_PORT.printf("MODE=high");
#endif    
  }
#if DEBUG_LVL >= 1
  DEBUG_PORT.printf("\n");
#endif    

  // DTR = false is counted as disconnected
  if (!dtr)
    if ( coding.bit_rate == 1200 ) TinyUSB_Port_EnterDFU();
}
#endif

// the setup routine runs once when you press reset:
void setup()
{
  pinMode(led, OUTPUT);
  digitalWrite(led, 0);
  
#if DEBUG_LVL >= 1
  DEBUG_PORT.begin(115200);
  DEBUG_PORT.println("\n\n\n\n\n\nDebug monitor !");
#endif

  // Set default baudrates
  ESP_PORT.begin(baud_rate);
  ESP_PORT.setTimeout(5);
  PC_PORT.begin(baud_rate);
  PC_PORT.setTimeout(5);

  // Set initial state of control pins
  // Normal start when the reset pin is released
  digitalWrite(PIN_ESP8285_RST, LOW);
  pinMode(PIN_ESP8285_RST, OUTPUT);
  digitalWrite(PIN_ESP8285_MODE, HIGH);
  pinMode(PIN_ESP8285_MODE, OUTPUT);
  delay(10);
  digitalWrite(PIN_ESP8285_RST, HIGH);

  while (!PC_PORT);
  digitalWrite(led, HIGH);
}

//
// The main loop were data is received from the host and then forwarded
// to the ESP8285 and vice versa.
//
void loop() {

  // Handle data from the USB port ment for the ESP8285
  if (PC_PORT.available()) {
    received = PC_PORT.readBytes(buffer, PACKET_SIZE);    
    ESP_PORT.write(buffer, received);

#if DEBUG_LVL >= 2
    if (port != 1) {
      port = 1;
      words = 0;
      DEBUG_PORT.println("\nFrom USB to ESP8285 ==================================");
    }
    for (int i=0;i<received;i++) {
      DEBUG_PORT.printf("%02x ", buffer[i]);
      if (++words > 15) {
        words = 0;
        DEBUG_PORT.println();
      }
    }
#endif
  }

  // Handle response data from the ESP8285
  if (ESP_PORT.available()) {
    received = ESP_PORT.readBytes(buffer, PACKET_SIZE);
    PC_PORT.write(buffer, received);
    
#if DEBUG_LVL >= 2
    if (port != 2) {
      port = 2;
      words = 0;
      DEBUG_PORT.println("\nFrom ESP8285 to USB ==================================");
    }
    for (int i=0;i<received;i++) {
      DEBUG_PORT.printf("%02x ", buffer[i]);
      if (++words > 15) {
        words = 0;
        DEBUG_PORT.println();
      }
    }
#endif
  }
}
